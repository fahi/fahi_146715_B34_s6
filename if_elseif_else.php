<?php
$t = date("H");

if ($t < "10") {
    echo "It is raining!";
} elseif ($t < "20") {
    echo "It is gloomy day!";
} else {
    echo "It is a sunny day!";
}
?>